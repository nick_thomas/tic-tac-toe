import { ReadLine } from "node:readline";
import readline from "readline";

interface MoveInterface {
    position: number,
    char: String,
    player: number,
}

class TicTacToe {
    ticTacToe: Array<String | undefined>;
    ticTacToeLayout: String;
    gameEnded: boolean;
    moveRegister: Array<MoveInterface>;
    currentPlayer: boolean;
    rl: ReadLine;
    constructor() {
        this.ticTacToe = [];
        this.ticTacToe[8] = undefined;
        this.ticTacToeLayout = ' ';
        this.currentPlayer = false;
        this.gameEnded = false;
        this.moveRegister = [];
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
    }
    updateLayout() {
        this.ticTacToeLayout = `${this.displayItem(this.ticTacToe[0])} | ${this.displayItem(this.ticTacToe[1])} | ${this.displayItem(this.ticTacToe[2])}
        ----------
        ${this.displayItem(this.ticTacToe[3])} | ${this.displayItem(this.ticTacToe[4])} | ${this.displayItem(this.ticTacToe[5])}
        ----------
        ${this.displayItem(this.ticTacToe[6])} | ${this.displayItem(this.ticTacToe[7])} | ${this.displayItem(this.ticTacToe[8])}`;
    }

    startGame() {
        this.displayLayout();

        // Listen to the input
        this.rl.on("line", (input) => {
            if (this.ticTacToe.length <= 9) {
                this.readMove(parseInt(input));
            }
        })
    }
    endGame() {

    }
    continuePlay() {
        this.displayLayout();
        this.processGame();
        if (!this.gameEnded) {
            this.currentPlayer = arguments[0] ? this.currentPlayer : !this.currentPlayer;
            console.log(`Player ${this.displayPlayer(this.currentPlayer)}, Your move! (position[1-9])`)
        }
    }
    // Todo
    processGame() {
        if (this.moveRegister.length >= 5) {
            const checkSet = new Set();
        }
    }
    // Helpers
    displayItem(item: String | undefined): String {
        return item === undefined ? ' ' : item
    }
    displayPlayer(player: boolean): number {
        return player ? 1 : 2;
    }
    displayLayout(): void {
        this.updateLayout();
        console.log(this.ticTacToeLayout);
    }
    getCharacter(player: boolean): String {
        return player ? 'X' : 'O';
    }
    readMove(position: number): void {
        let self = this;
        if ((position > 9) || (position < 1)) {
            self.moveError("Wrong Position!!!");
        }
        if (self.ticTacToe[(position - 1)] !== undefined) {
            console.log(self.ticTacToe[(position - 1)]);
            self.moveError("Position Occupied!!! ");
        } else {
            self.ticTacToe[(position - 1)] = self.getCharacter(self.currentPlayer);
            self.recordMove((position - 1), self.currentPlayer);
            self.continuePlay();
        }
    }
    moveError(message: String): void {
        console.log(`${arguments[0] ? arguments[0] : ''} Player ${this.displayPlayer(this.currentPlayer)}, Your move! (position 1-9);`)
    }
    recordMove(position: number, player: boolean): void {
        this.moveRegister.push({
            position,
            char: this.getCharacter(this.currentPlayer),
            player: this.displayPlayer(this.currentPlayer)
        })
    }
}